package me.ashtheking.gen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import me.ashtheking.gen.Colors.WorldColor;
import me.ashtheking.gen.export.Exporter;

public class MidpointGen
{

	public static int SIZE = 1;
	public static final Random RANDOM = new Random();
	public static Logger logger = Logger.getLogger("WorldGen");
	public static Panel worldPanel;
	public static long SEED;

	public static Midpoint height;
	public static Midpoint temp;
	public static Midpoint rain;
	public static Midpoint drain;

	static {
		SEED = RANDOM.nextLong();
		RANDOM.setSeed(SEED);
	}

	public static void main(String[] args) {
		Configuration.loadConfig("basic.cfg");
		Exporter.init();
		SEED = Long.parseLong("" + Configuration.config.getStringProperty("seed", "" + RANDOM.nextLong()).hashCode());
		RANDOM.setSeed(SEED);
		launchFrame(generate());
	}

	public static Tile[][] generate() {
		int size = (int) Math.pow(2, 7) + 1;

		double[][] heightmap = new double[size][size];
		double min = Configuration.config.getDoubleProperty("height-min", 0);
		double max = Configuration.config.getDoubleProperty("height-max", 1);
		height = new Midpoint(min, max);
		double initVar = height.initVar();
		RANDOM.setSeed(RANDOM.nextLong());
		height.divideGrid(heightmap, 0, 0, size, size, initVar, initVar, initVar, initVar);

		double[][] tempmap = new double[size][size];
		min = Configuration.config.getDoubleProperty("temp-min", 0);
		max = Configuration.config.getDoubleProperty("temp-max", 1);
		temp = new Midpoint(min, max);
		initVar = temp.initVar();
		System.out.println(initVar);
		RANDOM.setSeed(RANDOM.nextLong());
		temp.divideGrid(tempmap, 0, 0, size, size, initVar, initVar, initVar, initVar);

		double[][] rainmap = new double[size][size];
		min = Configuration.config.getDoubleProperty("rain-min", 0);
		max = Configuration.config.getDoubleProperty("rain-max", 1);
		rain = new Midpoint(min, max);
		initVar = rain.initVar();
		RANDOM.setSeed(RANDOM.nextLong());
		rain.divideGrid(rainmap, 0, 0, size, size, initVar, initVar, initVar, initVar);

		double[][] drainmap = new double[size][size];
		min = Configuration.config.getDoubleProperty("drain-min", 0);
		max = Configuration.config.getDoubleProperty("drain-max", 1);
		drain = new Midpoint(min, max);
		initVar = drain.initVar();
		RANDOM.setSeed(RANDOM.nextLong());
		drain.divideGrid(drainmap, 0, 0, size, size, initVar, initVar, initVar, initVar);

		RANDOM.setSeed(SEED);
		Tile[][] world = new Tile[size][size];
		for (int x = 0; x < size; x++)
			for (int y = 0; y < size; y++) {
				tempmap[x][y] += -0.75 * heightmap[x][y];
				tempmap[x][y] += 0.15 * (-0.015 * Math.pow(y - size / 2, 2) + 2);
				double humid = rainmap[x][y] / drainmap[x][y];
				double moist = Configuration.config.getDoubleProperty("moisture-max", 1);
				world[x][y] = new Tile(x, y, heightmap[x][y], tempmap[x][y], rainmap[x][y], drainmap[x][y]);
				world[x][y].moisture = Math.min(moist, humid);
			}
		throttle(world);

		double minH = ColorDefine.setOcean(world);
		int rivers = RANDOM.nextInt(200) + 20;
		Stats.stats.put("Rivers Generated: ", rivers);

		for (int k = 0; k < rivers; k++) {
			int x = 0, y = 0;
			while (true) {
				x = RANDOM.nextInt(world.length);
				y = RANDOM.nextInt(world.length);
				if (world[x][y].height >= minH - 1e-3 * minH && world[x][y].height < minH)
					break;
			}
			logger.log(Level.FINE, "River Generated at (" + x + ", " + y + ")");
			if (!ColorDefine.riverGen(world, x, y))
				k--;
		}
		return world;
	}

	public static void launchFrame(Tile[][] world) {
		SIZE = 800 / world.length;
		JFrame frame = new JFrame("World Generator");
		worldPanel = new Panel(world);
		frame.setContentPane(worldPanel);
		frame.setSize(world.length * SIZE + 25, world.length * SIZE + 40);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Stats.launchFrame();
	}

	public static void throttle(Tile[][] world) {
		int size = world.length;
		for (int x = 0; x < size; x++)
			for (int y = 0; y < size; y++) {
				world[x][y].height = Math.min(
						Math.max(world[x][y].height, Configuration.config.getDoubleProperty("height-min", 0)),
						Configuration.config.getDoubleProperty("height-max", 1));
				world[x][y].temp = Math.min(
						Math.max(world[x][y].temp, Configuration.config.getDoubleProperty("temp-min", 0)),
						Configuration.config.getDoubleProperty("temp-max", 1));
				world[x][y].rainfall = Math.min(
						Math.max(world[x][y].rainfall, Configuration.config.getDoubleProperty("rain-min", 0)),
						Configuration.config.getDoubleProperty("rain-max", 1));
				world[x][y].drainage = Math.min(
						Math.max(world[x][y].drainage, Configuration.config.getDoubleProperty("drain-min", 0)),
						Configuration.config.getDoubleProperty("drain-max", 1));
				double humid = world[x][y].moisture;
				double moist = Configuration.config.getDoubleProperty("moisture-max", 1);
				world[x][y].moisture = Math.min(moist, humid);
			}
	}

	public static class Midpoint
	{
		private double min;
		private double max;

		public Midpoint(double min, double max)
		{
			this.min = min;
			this.max = max;
		}

		public void divideGrid(double[][] points, double x, double y, double width, double height, double c1,
				double c2, double c3, double c4) {
			double Edge1, Edge2, Edge3, Edge4, Middle;

			double newWidth = Math.floor(width / 2);
			double newHeight = Math.floor(height / 2);

			if (width > 1 || height > 1) {
				Middle = ((c1 + c2 + c3 + c4) / 4) + displace(points.length, newWidth + newHeight); // Randomly displace
																									// the midpoint!
				Edge1 = ((c1 + c2) / 2); // Calculate the edges by averaging the two corners of each edge.
				Edge2 = ((c2 + c3) / 2);
				Edge3 = ((c3 + c4) / 2);
				Edge4 = ((c4 + c1) / 2);//
				// Make sure that the midpoint doesn't accidentally "randomly displaced" past the boundaries!
				Middle = rectify(Middle, min, max);
				Edge1 = rectify(Edge1, min, max);
				Edge2 = rectify(Edge2, min, max);
				Edge3 = rectify(Edge3, min, max);
				Edge4 = rectify(Edge4, min, max);
				// Do the operation over again for each of the four new grids.
				divideGrid(points, x, y, newWidth, newHeight, c1, Edge1, Middle, Edge4);
				divideGrid(points, x + newWidth, y, width - newWidth, newHeight, Edge1, c2, Edge2, Middle);
				divideGrid(points, x + newWidth, y + newHeight, width - newWidth, height - newHeight, Middle, Edge2,
						c3, Edge3);
				divideGrid(points, x, y + newHeight, newWidth, height - newHeight, Edge4, Middle, Edge3, c4);
			}
			else // This is the "base case," where each grid piece is less than the size of a pixel.
			{
				// The four corners of the grid piece will be averaged and drawn as a single pixel.
				double c = (c1 + c2 + c3 + c4) / 4;

				points[(int) (x)][(int) (y)] = c;
				if (width == 2) {
					points[(int) (x + 1)][(int) (y)] = c;
				}
				if (height == 2) {
					points[(int) (x)][(int) (y + 1)] = c;
				}
				if ((width == 2) && (height == 2)) {
					points[(int) (x + 1)][(int) (y + 1)] = c;
				}
			}
		}

		private double rectify(double iNum, double min, double max) {
			if (iNum < min)
				iNum = min;
			else if (iNum > max)
				iNum = max;
			return iNum;
		}

		private double displace(double array, double smallSize) {

			double Max = smallSize / array * 0.75;
			return (initVar() - 0.5) * Max;
		}

		public double initVar() {
			return ((RANDOM.nextDouble() * (max - min)) + min);
		}
	}

	public static class Tile
	{

		public int x, y;
		public double height;
		public double temp;
		public double rainfall;
		public double drainage;
		public double moisture;
		public boolean ocean;
		public boolean river;
		public boolean lake;

		public Tile(int x, int y)
		{
			this(x, y, 0, 0, 0, 0);
		}

		public Tile(int x, int y, double h)
		{
			this(x, y, h, 0, 0, 0);
		}

		public Tile(int x, int y, double h, double t)
		{
			this(x, y, h, t, 0, 0);
		}

		public Tile(int x, int y, double h, double t, double r)
		{
			this(x, y, h, t, r, 0);
		}

		public Tile(int x, int y, double h, double t, double r, double d)
		{
			this.x = x;
			this.y = y;
			this.height = h;
			this.temp = t;
			this.rainfall = r;
			this.drainage = d;
		}
	}

	public static class Stats
	{
		public static Map<String, Number> stats = new TreeMap<String, Number>();
		private static Map<String, JLabel> val = new TreeMap<String, JLabel>();

		public static void launchFrame() {
			final JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(stats.size() + 5, 2));
			panel.add(new JLabel("World Name: "));
			panel.add(new JLabel("--TBD--"));
			panel.add(new JLabel("RNG Seed: "));
			panel.add(new JLabel("" + SEED));
			JButton loadConf = new JButton("Load Config");
			loadConf.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Configuration.loadConfig();
				}
			});
			panel.add(new JLabel("Configuration: "));
			panel.add(loadConf);
			JButton generate = new JButton("Generate");
			generate.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					worldPanel.formPicture(generate(), 4);
				}
			});
			panel.add(new JLabel("New World: "));
			panel.add(generate);

			for (String s : stats.keySet()) {
				if (stats.get(s) instanceof Double)
					stats.put(s, (int) (stats.get(s).doubleValue() * 10000) / 10000.0);
			}
			for (String s : stats.keySet()) {
				JLabel stat = new JLabel("" + stats.get(s));
				panel.add(new JLabel(s));
				panel.add(stat);
				val.put(s, stat);

				logger.log(Level.FINE, "" + s + ": " + stats.get(s));
			}
			Timer t = new Timer(100, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for (String s : stats.keySet()) {
						JLabel valu = val.get(s);
						if (stats.get(s) instanceof Double)
							stats.put(s, (int) (stats.get(s).doubleValue() * 10000) / 10000.0);
						valu.setText("" + stats.get(s));
						logger.log(Level.FINE, "" + s + ": " + stats.get(s));
					}
				}
			});
			t.start();

			JButton export = new JButton("Export World");
			export.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Exporter.showDialog(worldPanel.world);
				}
			});
			panel.add(new JLabel("Export"));
			panel.add(export);

			JFrame frame = new JFrame("World Information");
			frame.setContentPane(panel);
			int height = stats.size() * 50 + 50;
			if (height > (Math.pow(2, 8) + 1) * SIZE + 40)
				height = (int) (Math.pow(2, 8) + 1) * SIZE + 40;
			frame.setSize(450, height);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
	}

	public static class ColorDefine
	{
		public static double ocean;

		public static double max(Tile[][] world, int t) {
			double max = -10;
			for (int x = 0; x < world.length; x++)
				for (int y = 0; y < world.length; y++) {
					double k = (t == 0 ? world[x][y].height : (t == 1 ? world[x][y].temp
							: (t == 2 ? world[x][y].drainage : (t == 3 ? world[x][y].rainfall : world[x][y].moisture))));
					if (k > max)
						max = k;
				}
			return max;
		}

		public static double min(Tile[][] world, int t) {
			double min = 100;
			for (int x = 0; x < world.length; x++)
				for (int y = 0; y < world.length; y++) {
					double k = (t == 0 ? world[x][y].height : (t == 1 ? world[x][y].temp
							: (t == 2 ? world[x][y].drainage : (t == 3 ? world[x][y].rainfall : world[x][y].moisture))));
					if (k < min)
						min = k;
				}
			return min;
		}

		public static double setOcean(Tile[][] world) {
			double max = max(world, 0);
			double min = min(world, 0);
			double ran = max - min;
			double cut = min + ran * Configuration.config.getDoubleProperty("ocean-percent", 0.35);
			for (int x = 0; x < world.length; x++)
				for (int y = 0; y < world.length; y++)
					if (world[x][y].height < cut)
						world[x][y].ocean = true;
			Stats.stats.put("Ocean Shore:", cut);
			Stats.stats.put("Ocean Floor:", min);
			Stats.stats.put("Ocean Percent:", Configuration.config.getDoubleProperty("ocean-percent", 0.35));
			return cut;
		}

		public static boolean riverGen(Tile[][] world, int x, int y) {
			//world[x][y].river = true;
			int chance = 5;
			int[] move = getDir(world, new int[] { 0, 1 });
			double elevation = world[x][y].height;
			int loop = 0;
			ArrayList<int[]> locs = new ArrayList<int[]>();
			while (true) {
				if ((x + move[0]) > -1 && (x + move[0]) < world.length && (y + move[1]) > -1
						&& (y + move[1]) < world.length)
					if (!world[x + move[0]][y + move[1]].ocean)
						break;
				move = getDir(world, move);
				loop++;
				if (loop >= 20)
					return false;
			}
			loop = 0;
			while (true) {
				int stop = RANDOM.nextInt(1000) + 20;
				if (stop < chance)
					break;
				else
					chance += 5;

				int newX = x + move[0];
				int newY = y + move[1];

				if (newX < 0 || newX >= world.length || newY < 0 || newY >= world.length) {
					//locs.add(new int[] { x, y });
					//world[x][y].river = true;
					//world[x][y].moisture *= 1.25;
					break;
				}

				if (world[newX][newY].height < elevation) {
					if (RANDOM.nextDouble() > 0.4) {
						move = getDir(world, move);
						continue;
					}
				}
				else {
					x = newX;
					y = newY;
					locs.add(new int[] { x, y });
					elevation = world[x][y].height;
					move = getDir(world, move);
				}
			}
			if (locs.size() > 9) {
				for (int[] a : locs) {
					x = a[0];
					y = a[1];
					world[x][y].river = true;
					world[x][y].moisture *= 1.25;
					world[x][y].height *= 0.95;
				}
				//Lake Gen
				locs = new ArrayList<int[]>();
				int size = RANDOM.nextInt(5) + 2;
				for (int a = 0 - size; a < 0 + size; a++)
					for (int b = 0 - (RANDOM.nextInt(5) + 2); b < 0 + RANDOM.nextInt(5) + 2; b++)
						if ((x + a) < world.length && (x + a) > -1)
							if ((y + b) < world[0].length && (y + b) > -1)
								if (world[x + a][y + b].height < world[x][y].height + 9)
									locs.add(new int[] { x + a, y + b });
				for (int[] a : locs) {
					x = a[0];
					y = a[1];
					if (world[x][y].ocean)
						break;
					world[x][y].river = false;
					world[x][y].lake = true;
					world[x][y].moisture *= Math.min(RANDOM.nextDouble() + 0.75, 1.25);
					world[x][y].height *= 0.95;
				}
			}
			return true;
		}

		public static int[] getDir(Tile[][] world, int[] move) {
			int j = RANDOM.nextInt(100);
			if (j > 25)
				return move;
			else {
				j = RANDOM.nextInt(100);
				int m, delta;
				int k = RANDOM.nextInt(100);
				if (j < 50) {
					m = move[0] + 1;
					delta = (k < 50 ? -1 : 1);
					m += delta;
					m = (m % 2) - 1;
					m = (m > 1 ? 1 : (m < -1 ? -1 : m));
					return new int[] { m, move[1] };
				}
				else {
					m = move[1] + 1;
					delta = (k < 50 ? -1 : 1);
					m += delta;
					m = (m % 2) - 1;
					m = (m > 1 ? 1 : (m < -1 ? -1 : m));
					return new int[] { move[0], m };
				}
			}
		}

		public static char[][] charMap(Tile[][] world, int t) {
			double max = max(world, t);
			double min = min(world, t);
			double scale = (max - min);
			logger.log(Level.INFO, "Max: " + max + "\nMin: " + min);
			char[][] display = new char[world.length][world.length];

			for (int x = 0; x < world.length; x++)
				for (int y = 0; y < world.length; y++) {
					double k = (t == 0 ? world[x][y].height : (t == 1 ? world[x][y].temp
							: (t == 2 ? world[x][y].drainage : world[x][y].rainfall)))
							- min;
					if (k < scale * 0.4)
						display[x][y] = '~';
					else if (k >= scale * 0.4 && k < scale * 0.45)
						display[x][y] = ',';
					else if (k >= scale * 0.45 && k < scale * 0.6)
						display[x][y] = '.';
					else if (k >= scale * 0.6 && k < scale * 0.7)
						display[x][y] = '#';
					else if (k >= scale * 0.7 && k < scale * 0.8)
						display[x][y] = '/';
					else if (k >= scale * 0.8 && k < scale * 0.9)
						display[x][y] = '^';
					else if (k >= scale * 0.9)
						display[x][y] = '`';
				}
			return display;

		}

		public static Color[][] worldMap(Tile[][] world) {
			Color[][] display = new Color[world.length][world.length];

			double maxH = max(world, 0);
			double minH = setOcean(world);
			double ranH = 100;
			ocean = minH;

			double maxT = max(world, 1);
			double minT = min(world, 1);
			double ranT = 100;

			double maxD = max(world, 2);
			double minD = min(world, 2);
			double ranD = 100;

			double maxR = max(world, 3);
			double minR = min(world, 3);
			// double ranR = maxR - minR;

			double maxM = max(world, 4);
			double minM = min(world, 4);
			double ranM = Configuration.config.getDoubleProperty("rain-max", 1)
					/ (Configuration.config.getDoubleProperty("drain-min", 0) + 0.1);

			Stats.stats.put("Land Height Max:", maxH);
			Stats.stats.put("Land Height Min:", minH);
			Stats.stats.put("Temperature Max:", maxT);
			Stats.stats.put("Temperature Min:", minT);
			Stats.stats.put("Drainage Max:", maxD);
			Stats.stats.put("Drainage Min:", minD);
			Stats.stats.put("Rainfall Max:", maxR);
			Stats.stats.put("Rainfall Min:", minR);
			Stats.stats.put("Moisture Max:", maxM);
			Stats.stats.put("Moisture Min:", minM);

			for (WorldColor c : Colors.colors)
				c.usage = 0;

			for (int x = 0; x < world.length; x++) {
				for (int y = 0; y < world.length; y++) {

					double h = world[x][y].height;
					double t = world[x][y].temp;
					double d = world[x][y].drainage;
					// double r = world[x][y].rainfall;
					double m = world[x][y].moisture;
					// if (m > 100)
					// System.out.println("" + m + " " + x + " " + y);
					// h -= minH;
					// t -= minT;
					// d -= minD;
					// r -= minR;
					// m -= minM;

					if (world[x][y].ocean || world[x][y].river || world[x][y].lake) {
						if (t < ranT * 0.33)
							display[x][y] = Colors.ICE.getColor();
						else if (d < ranD * 0.25)
							display[x][y] = Colors.MARSH.getColor();
						else if (world[x][y].river)
							display[x][y] = Colors.RIVER.getColor();
						else if (world[x][y].lake && !world[x][y].ocean)
							display[x][y] = Colors.LAKE.getColor();
						else
							display[x][y] = Colors.OCEAN.getColor();
					}
					else if (h > ranH * 0.75) {
						if (m < ranM * 0.16)
							display[x][y] = Colors.SCORCHED.getColor();
						else if (m >= ranM * 0.16 && m < ranM * 0.33)
							display[x][y] = Colors.BARE.getColor();
						else if (m >= ranM * 0.33 && m < ranM * 0.75)
							display[x][y] = Colors.TUNDRA.getColor();
						else if (m >= ranM * 0.75)
							display[x][y] = Colors.SNOW.getColor();
					}
					else if (h > ranH * 0.5) {
						if (m < ranM * 0.33)
							display[x][y] = Colors.TEMPERATE_DESERT.getColor();
						else if (m >= ranM * 0.33 && m < ranM * 0.66)
							display[x][y] = Colors.SHRUBLAND.getColor();
						else if (m >= ranM * 0.66)
							display[x][y] = Colors.TAIGA.getColor();
					}
					else if (h > ranH * 0.25) {
						if (m < ranM * 0.16)
							display[x][y] = Colors.TEMPERATE_DESERT.getColor();
						else if (m >= ranM * 0.16 && m < ranM * 0.5)
							display[x][y] = Colors.GRASSLAND.getColor();
						else if (m >= ranM * 0.5 && m < ranM * 0.83)
							display[x][y] = Colors.TEMPERATE_DECIDUOUS_FOREST.getColor();
						else if (m >= ranM * 0.83)
							display[x][y] = Colors.TEMPERATE_RAIN_FOREST.getColor();
					}
					else if (h > ranH * 1e-12) {
						if (m < ranM * 0.16)
							display[x][y] = Colors.SUBTROPICAL_DESERT.getColor();
						else if (m >= ranM * 0.16 && m < ranM * 0.33)
							display[x][y] = Colors.GRASSLAND.getColor();
						else if (m >= ranM * 0.33 && m < ranM * 0.66)
							display[x][y] = Colors.TROPICAL_SEASONAL_FOREST.getColor();
						else if (m >= ranM * 0.66)
							display[x][y] = Colors.TROPICAL_RAIN_FOREST.getColor();
					}
					else if (h > minH * 0.98)
						display[x][y] = Colors.BEACH.getColor();

					//					if((int)Math.abs(world[x][y].height - minH) < 4) {
					//						display[x][y] = Colors.BEACH.getColor();
					//					}

					// System.out.print((int)world[x][y].temp + " ");
				}
				// System.out.println();
			}
			for (WorldColor wc : Colors.colors)
				Stats.stats.put("Tile " + wc.name + ":", wc.usage);

			return display;
		}

		public static Color[][] layerMap(Tile[][] world, int t) {
			double max = max(world, t);
			double min = min(world, t);
			double r = max(world, 3) / min(world, 2);
			logger.log(Level.FINE, "Max: " + max + "\nMin: " + min);
			Color[][] display = new Color[world.length][world.length];

			for (int x = 0; x < world.length; x++) {
				for (int y = 0; y < world.length; y++) {
					double k;
					double c;
					if (t == 5) {
						k = world[x][y].moisture;
						c = Math.abs(k / r) * 255;
					}
					else {
						k = (t == 0 ? world[x][y].height : (t == 1 ? world[x][y].temp : (t == 2 ? world[x][y].drainage
								: world[x][y].rainfall)));
						c = Math.abs(k / 100) * 255;
					}
					int color = (int) Math.max(Math.min(c, 255), 0);
					// System.out.print(c + "|" + color + " ");
					display[x][y] = new Color(color, color, color);
				}
				// System.out.println();
			}
			return display;
		}
	}

	public static class Panel extends JPanel
	{

		private static final long serialVersionUID = 8251616948285161925L;
		private Color[][] colors;
		public Tile[][] world;

		public Panel(Tile[][] world)
		{
			throttle(world);
			formPicture(world, 4);
			addMouseListener(new Mouse());
			addKeyListener(new Key());
			setFocusable(true);
			Timer t = new Timer(1500, new Listener());
			t.start();
		}

		@Override
		public void paint(Graphics graph) {
			for (int x = 0; x < colors.length; x++)
				for (int y = 0; y < colors.length; y++) {
					graph.setColor(Color.black);
					graph.fillRect(x * SIZE, (y - 1) * SIZE, SIZE * 2, SIZE * 2);
					graph.setColor(colors[x][y]);
					graph.fillRect(x * SIZE, (y - 1) * SIZE, SIZE * 2, SIZE * 2);
					graph.setColor(Color.black);
					// graph.drawString("" + display[x][y], x * 10, y * 10);
				}
		}

		public static void printWorld(char[][] world) {
			System.out.print("+ ");
			for (int x = 0; x < world.length; x++)
				System.out.print("_ ");
			System.out.println("+");
			for (int x = 0; x < world.length; x++) {
				System.out.print("| ");
				for (int y = 0; y < world.length; y++)
					System.out.print(world[x][y] + " ");
				System.out.println("|");
			}
			System.out.print("+ ");
			for (int x = 0; x < world.length; x++)
				System.out.print("_ ");
			System.out.println("+");
		}

		public void formPicture(Tile[][] world, int t) {
			this.world = world;
			switch (t) {
			case 4:
				colors = ColorDefine.worldMap(world);
				break;
			default:
				colors = ColorDefine.layerMap(world, t);
				break;
			}
		}

		private class Listener implements ActionListener
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getGraphics() != null)
					paint(getGraphics());
			}
		}

		private class Mouse extends MouseAdapter
		{
			@Override
			public void mouseClicked(MouseEvent event) {
				formPicture(generate(), 4);
			}
		}

		private class Key extends KeyAdapter
		{
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.getKeyCode() == KeyEvent.VK_H)
					formPicture(world, 0);
				if (event.getKeyCode() == KeyEvent.VK_T)
					formPicture(world, 1);
				if (event.getKeyCode() == KeyEvent.VK_D)
					formPicture(world, 2);
				if (event.getKeyCode() == KeyEvent.VK_R)
					formPicture(world, 3);
				if (event.getKeyCode() == KeyEvent.VK_G)
					formPicture(world, 4);
				if (event.getKeyCode() == KeyEvent.VK_M)
					formPicture(world, 5);
			}
		}
	}

}