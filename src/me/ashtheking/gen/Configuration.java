package me.ashtheking.gen;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Configuration
{

	static final Logger logger = Logger.getLogger("WorldGen");
	public static PropertyManager config;

	public static void loadConfig() {
		JFileChooser chooser = new JFileChooser(new File("./config/"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("WorldGen Configuration Files", "cfg");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			logger.log(Level.INFO, "You chose to load this file: " + chooser.getSelectedFile().getName());
			config = new PropertyManager(chooser.getSelectedFile().getName());
		}
	}

	public static void loadConfig(String file) {
		config = new PropertyManager(file);
	}
}
