package me.ashtheking.gen;

import java.awt.Color;
import java.util.ArrayList;

public class Colors
{
	public static final WorldColor OCEAN = new WorldColor(0x44447a, "Water-Ocean");
	public static final WorldColor COAST = new WorldColor(0x33335a, "Shore-Ocean");
	public static final WorldColor LAKESHORE = new WorldColor(0x225588, "Shore-Lake");
	public static final WorldColor LAKE = new WorldColor(0x336699, "Water-Lake");
	public static final WorldColor RIVER = new WorldColor(0x225588, "Water-River");
	public static final WorldColor MARSH = new WorldColor(0x2f6666, "Water-Marsh");
	public static final WorldColor ICE = new WorldColor(0x99ffff, "Water-Ice");
	public static final WorldColor BEACH = new WorldColor(0xa09077, "Shore-Beach");
	public static final WorldColor ROAD1 = new WorldColor(0x442211, "Road1");
	public static final WorldColor ROAD2 = new WorldColor(0x553322, "Road2");
	public static final WorldColor ROAD3 = new WorldColor(0x664433, "Road3");
	public static final WorldColor BRIDGE = new WorldColor(0x686860, "Road-Bridge");
	public static final WorldColor LAVA = new WorldColor(0xcc3333, "Water-Lava");

	// Terrain
	public static final WorldColor SNOW = new WorldColor(0xffffff, "Snow");
	public static final WorldColor TUNDRA = new WorldColor(0xbbbbaa, "Tundra");
	public static final WorldColor BARE = new WorldColor(0x888888, "Barren");
	public static final WorldColor SCORCHED = new WorldColor(0x555555, "Scorched");
	public static final WorldColor TAIGA = new WorldColor(0x99aa77, "Taiga");
	public static final WorldColor SHRUBLAND = new WorldColor(0x889977, "Shrubland");
	public static final WorldColor TEMPERATE_DESERT = new WorldColor(0xc9d29b, "Desert-Temperate");
	public static final WorldColor TEMPERATE_RAIN_FOREST = new WorldColor(0x448855, "Forest-Rain-Temperate");
	public static final WorldColor TEMPERATE_DECIDUOUS_FOREST = new WorldColor(0x679459, "Forest-Decidious");
	public static final WorldColor GRASSLAND = new WorldColor(0x88aa55, "Grassland");
	public static final WorldColor SUBTROPICAL_DESERT = new WorldColor(0xd2b98b, "Desert-Subtropical");
	public static final WorldColor TROPICAL_RAIN_FOREST = new WorldColor(0x337755, "Forest-Rain-Tropical");
	public static final WorldColor TROPICAL_SEASONAL_FOREST = new WorldColor(0x559944, "Forest-Seasonal");

	protected static ArrayList<WorldColor> colors = new ArrayList<WorldColor>();

	static {
		colors.add(OCEAN);
		// colors.add(COAST);
		// colors.add(LAKESHORE);
		colors.add(LAKE);
		colors.add(RIVER);
		colors.add(MARSH);
		colors.add(ICE);
		colors.add(BEACH);
		// colors.add(ROAD1);
		// colors.add(ROAD2);
		// colors.add(ROAD3);
		// colors.add(BRIDGE);
		// colors.add(LAVA);
		colors.add(SNOW);
		colors.add(TUNDRA);
		colors.add(BARE);
		colors.add(SCORCHED);
		colors.add(TAIGA);
		colors.add(SHRUBLAND);
		colors.add(TEMPERATE_DESERT);
		colors.add(TEMPERATE_RAIN_FOREST);
		colors.add(TEMPERATE_DECIDUOUS_FOREST);
		colors.add(GRASSLAND);
		colors.add(SUBTROPICAL_DESERT);
		colors.add(TROPICAL_RAIN_FOREST);
		colors.add(TROPICAL_SEASONAL_FOREST);
	}

	static class WorldColor extends Color
	{
		private static final long serialVersionUID = -2816465047324801950L;
		public String name;
		public int usage;

		public WorldColor(int x, String n)
		{
			super(x);
			usage = 0;
			name = n;
		}

		public Color getColor() {
			usage++;
			return this;
		}
	}
}
