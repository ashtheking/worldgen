package me.ashtheking.gen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertyManager
{

	public static Logger logger = Logger.getLogger("WorldGen");
	private Properties serverProperties;
	private File serverPropertiesFile;

	public PropertyManager(String file)
	{
		File f = new File(file);
		serverProperties = new SortedProperties();
		serverPropertiesFile = f;
		String s = serverPropertiesFile.getAbsolutePath();
		String str = s.substring(0, s.indexOf(serverPropertiesFile.getPath()) - 1) + "/config/" + file;
		f = new File(str);
		if (f.exists()) {
			try {
				serverProperties.load(new FileInputStream(f));
			} catch (Exception exception) {
				logger.log(Level.WARNING, (new StringBuilder()).append("Failed to load ").append(file).toString(),
						exception);
				generateNewProperties();
			}
		}
		else {
			try {
				logger.log(Level.WARNING, (new StringBuilder()).append(file).append(" does not exist").toString());
				generateNewProperties();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void generateNewProperties() {
		logger.log(Level.INFO, "Generating new properties file");
		saveProperties();
	}

	public void saveProperties() {
		try {
			if (!serverPropertiesFile.exists()) {
				String s = serverPropertiesFile.getAbsolutePath();
				File f = new File(s.substring(0, s.indexOf(serverPropertiesFile.getPath()) - 1) + "/config/");
				f.mkdirs();
				serverPropertiesFile = new File(f.getAbsolutePath() + "/" + serverPropertiesFile.getPath());
			}
			serverProperties.store(new FileOutputStream(serverPropertiesFile), "WorldGen World Configuration");
		} catch (Exception exception) {
			logger.log(Level.WARNING, (new StringBuilder()).append("Failed to save ").append(serverPropertiesFile)
					.toString(), exception);
			generateNewProperties();
		}
	}

	public String getStringProperty(String s, String s1) {
		if (!serverProperties.containsKey(s)) {
			serverProperties.setProperty(s, s1);
			saveProperties();
		}
		return serverProperties.getProperty(s, s1);
	}

	public int getIntProperty(String s, int i) {
		try {
			return Integer.parseInt(getStringProperty(s, (new StringBuilder()).append("").append(i).toString()));
		} catch (Exception exception) {
			serverProperties.setProperty(s, (new StringBuilder()).append("").append(i).toString());
		}
		return i;
	}

	public double getDoubleProperty(String s, double d) {
		try {
			return Double.parseDouble(getStringProperty(s, (new StringBuilder()).append("").append(d).toString()));
		} catch (Exception exception) {
			serverProperties.setProperty(s, (new StringBuilder()).append("").append(d).toString());
		}
		return d;
	}

	public boolean getBooleanProperty(String s, boolean flag) {
		try {
			return Boolean.parseBoolean(getStringProperty(s, (new StringBuilder()).append("").append(flag).toString()));
		} catch (Exception exception) {
			serverProperties.setProperty(s, (new StringBuilder()).append("").append(flag).toString());
		}
		return flag;
	}

	public void setBooleanProperty(String s, boolean flag) {
		serverProperties.setProperty(s, (new StringBuilder()).append("").append(flag).toString());
		saveProperties();
	}

	public void setIntProperty(String s, int flag) {
		serverProperties.setProperty(s, (new StringBuilder()).append("").append(flag).toString());
		saveProperties();
	}

	private class SortedProperties extends Properties
	{
		private static final long serialVersionUID = 7116611512443000753L;

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Enumeration keys() {
			Enumeration<Object> keysEnum = super.keys();
			Vector<String> keyList = new Vector<String>();
			while (keysEnum.hasMoreElements()) {
				keyList.add((String) keysEnum.nextElement());
			}
			Collections.sort(keyList);
			return keyList.elements();
		}

	}

}
