package me.ashtheking.gen.old;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class WorldGen
{
	public static int SIZE = 5;

	public static void main(String[] args) throws Exception {

		double[][] world = getRandomArray2((int) Math.pow(2, 6) + 1);
		System.setOut(new PrintStream(new FileOutputStream(new File("log"))));
		printWorld(world);
		// world = boundArray(world);
		world = slopeArray(world);
		launchFrame(world);
		new File("log").delete();
	}

	public static char[][] printWorld(double[][] world) {
		double max = max(world);
		boolean[][] check = new boolean[world.length][world.length];
		// check = floodOcean(check, world, 0, 0, max * 0.4);
		char[][] display = new char[world.length][world.length];
		System.out.print("+ ");
		for (int x = 0; x < world.length; x++)
			System.out.print("- ");
		System.out.println("+");

		for (int x = 0; x < world.length; x++) {
			System.out.print("| ");
			for (int y = 0; y < world.length; y++) {
				if (world[x][y] < max * 0.4)
					if (!check[x][y]) {
						System.out.print("~");
						display[x][y] = '~';
					}
					else {
						System.out.print("-");
						display[x][y] = '-';
					}
				else if (world[x][y] >= max * 0.4 && world[x][y] < max * 0.6) {
					System.out.print(".");
					display[x][y] = '.';
				}
				else if (world[x][y] >= max * 0.6 && world[x][y] < max * 0.8) {
					System.out.print("#");
					display[x][y] = '#';
				}
				else if (world[x][y] >= max * 0.8) {
					System.out.print("^");
					display[x][y] = '^';
				}
				System.out.print(" ");
			}
			System.out.println("|");
		}

		System.out.print("+ ");
		for (int x = 0; x < world.length; x++)
			System.out.print("- ");
		System.out.println("+");
		return display;
	}

	public static double max(double[][] world) {
		double max = -10;
		for (int x = 0; x < world.length; x++)
			for (int y = 0; y < world.length; y++)
				if (world[x][y] > max)
					max = world[x][y];
		return max;
	}

	public static double[][] getRandomArray(int size) {
		double[][] array = new double[size][size];
		for (int x = 0; x < size; x++)
			for (int y = 0; y < size; y++)
				array[x][y] = Math.random();
		for (int x = 0; x < size; x++)
			for (int y = 0; y < size; y++)
				array[x][y] += Math.random() * 0.5;
		return array;
	}

	public static double[][] getRandomArray2(int size) {
		double[][] array = getRandomArray(size);
		double k = Math.random();
		array[0][0] = k;
		array[array.length - 1][0] = k;
		array[0][array.length - 1] = k;
		array[array.length - 1][array.length - 1] = k;
		midpoint(array, 0, 0, array.length, array.length, array[0][0], array[array.length - 1][0],
				array[array.length - 1][array.length - 1], array[0][array.length - 1]);
		return array;
	}

	public static void midpoint(double[][] array, int x, int y, int width, int height, double c1, double c2, double c3,
			double c4) {
		double edge1, edge2, edge3, edge4, middle;
		int newWidth = width / 2;
		int newHeight = height / 2;
		if (width > 1 || height > 1) {
			middle = Math.abs((c1 + c2 + c3 + c4) / 4.0) + displace(array, newWidth + newHeight);
			edge1 = (c1 + c2) / 2;
			edge2 = (c2 + c3) / 2;
			edge3 = (c3 + c4) / 2;
			edge4 = (c4 + c1) / 2;
			midpoint(array, x, y, newWidth, newHeight, c1, edge1, middle, edge4);
			midpoint(array, x + newWidth, y, newWidth, newHeight, edge1, c2, edge2, middle);
			midpoint(array, x + newWidth, y + newHeight, newWidth, newHeight, middle, edge2, c3, edge3);
			midpoint(array, x, y + newHeight, newWidth, newHeight, edge4, middle, edge3, c4);
		}
		else {
			double k = Math.abs((c1 + c2 + c3 + c4) / 4.0) + displace(array, newWidth + newHeight);
			// k *= 3.75;
			if (k < 0)
				k = 0;
			array[x][y] = k;
		}
	}

	public static double displace(double[][] array, float num) {
		double max = num / (double) (array.length * 2) * 3;
		return (Math.random() - 0.5f) * max;
	}

	public static double[][] boundArray(double[][] world) {
		for (int x = 0; x < world.length; x += (world.length - 1))
			for (int y = 0; y < world.length; y++)
				world[x][y] = 0.0;
		for (int x = 0; x < world.length; x++)
			for (int y = 0; y < world.length; y += (world.length - 1))
				world[x][y] = 0.0;
		return world;
	}

	public static double[][] slopeArray(double[][] grid) {
		boolean b = false;
		while (!b) {
			b = true;
			for (int x = 0; x < grid.length; x++)
				for (int y = 0; y < grid.length; y++) {
					if (x - 1 >= 0) {
						if (grid[x - 1][y] > grid[x][y] + 0.1) {
							grid[x - 1][y] = grid[x][y] + 0.1;
							b = false;
						}
						if (y - 1 >= 0)
							if (grid[x - 1][y - 1] > grid[x][y] + 0.1) {
								grid[x - 1][y - 1] = grid[x][y] + 0.1;
								b = false;
							}
						if (y + 1 < grid.length)
							if (grid[x - 1][y + 1] > grid[x][y] + 0.1) {
								grid[x - 1][y + 1] = grid[x][y] + 0.1;
								b = false;
							}
					}
					if (y - 1 >= 0)
						if (grid[x][y - 1] > grid[x][y] + 0.1) {
							grid[x][y - 1] = grid[x][y] + 0.1;
							b = false;
						}
					if (x + 1 < grid.length) {
						if (grid[x + 1][y] > grid[x][y] + 0.1) {
							grid[x + 1][y] = grid[x][y] + 0.1;
							b = false;
						}
						if (y + 1 < grid.length)
							if (grid[x + 1][y + 1] > grid[x][y] + 0.1) {
								grid[x + 1][y + 1] = grid[x][y] + 0.1;
								b = false;
							}
					}
					if (y + 1 < grid.length)
						if (grid[x][y + 1] > grid[x][y] + 0.1) {
							grid[x][y + 1] = grid[x][y] + 0.1;
							b = false;
						}
				}
		}
		return grid;
	}

	public static void launchFrame(double[][] world) {
		JFrame frame = new JFrame("World Generator");
		frame.setContentPane(new Panel(printWorld(world), world));
		frame.setSize(world.length * SIZE + 25, world.length * SIZE + 40);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static boolean[][] floodOcean(boolean[][] check, double[][] world, int x, int y, double land) {
		if (x > world.length - 1 || y > world.length - 1 || x < 0 || y < 0)
			return check;
		if (check[x][y] || !(world[x][y] < land))
			return check;
		check[x][y] = true;
		check = floodOcean(check, world, x + 1, y, land);
		check = floodOcean(check, world, x + 1, y + 1, land);
		check = floodOcean(check, world, x + 1, y - 1, land);

		check = floodOcean(check, world, x - 1, y, land);
		check = floodOcean(check, world, x - 1, y + 1, land);
		check = floodOcean(check, world, x - 1, y - 1, land);

		check = floodOcean(check, world, x, y + 1, land);
		check = floodOcean(check, world, x, y - 1, land);
		return check;
	}

	public static class Panel extends JPanel
	{

		private static final long serialVersionUID = 8251616948285161925L;
		private Color[][] colors;
		private char[][] display;

		public Panel(char[][] printed, double[][] world)
		{
			display = printed;
			colors = new Color[world.length][world.length];
			for (int x = 0; x < world.length; x++)
				for (int y = 0; y < world.length; y++) {
					if (display[x][y] == '~')
						colors[x][y] = Color.blue.darker();
					if (display[x][y] == '.')
						colors[x][y] = Color.yellow.darker();
					if (display[x][y] == '#')
						colors[x][y] = Color.green.darker();
					if (display[x][y] == '^')
						colors[x][y] = Color.gray;
				}
		}

		@Override
		public void paint(Graphics graph) {
			for (int x = 0; x < display.length; x++)
				for (int y = 0; y < display.length; y++) {
					graph.setColor(Color.black);
					graph.fillRect(x * SIZE, (y - 1) * SIZE, SIZE * 2, SIZE * 2);
					graph.setColor(colors[x][y]);
					graph.fillRect(x * SIZE, (y - 1) * SIZE, SIZE * 2, SIZE * 2);
					graph.setColor(Color.black);
					// graph.drawString("" + display[x][y], x * 10, y * 10);
				}
		}
	}
}
