package me.ashtheking.gen.old;

import java.util.Random;

public class DwarfGen
{
	public static Random rand = new Random();
	public static double H_VAL = 0.5;

	public static void main(String[] args) {
		int size = (int) Math.pow(2, 6) + 1;
		int stride = size / 2;
		int x1 = 0, y1 = 0, x2 = size - 1, y2 = 0, x3 = 0, y3 = size - 1, x4 = size - 1, y4 = size - 1;
		double[][] array = new double[size][size];
		double maxRough = 1.0;

		array[x1][y1] = array[x2][y2] = array[x3][y3] = array[x4][y4] = rand.nextDouble();
		while (stride > 0) {

			// Create Squares
			for (int i = stride; i < size; i += stride) {
				for (int j = stride; j < size; j += stride) {
					diamond(array, maxRough, i, j, stride);
					j += stride;
				}
				i += stride;
			}

			// Create Diamonds
			boolean oddline = false;
			for (int i = 0; i < size; i += stride) {
				oddline = (oddline == false);
				for (int j = 0; j < size; j += stride) {
					if ((oddline) && !(j == 0))
						j += stride;
					square(array, maxRough, i, j, stride);
					if (i == 0)
						array[array.length - 1][j] = array[i][j];
					if (j == 0)
						array[i][array.length - 1] = array[i][j];
				}
			}

			maxRough *= H_VAL;
			stride /= 2;

		}
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				System.out
						.print((array[i][j] < 0.2 ? "~" : (array[i][j] < 0.5 ? "." : (array[i][j] < 0.7 ? "#" : "^"))));
			System.out.println();
		}
	}

	/*
	 * +-+-+-+ |1| |2| +-+-+-+ | |M| | +-+-+-+ |3| |4| +-+-+-+
	 */
	public static void diamond(double[][] array, double maxRough, int x, int y, int stride) {
		int x1 = x - stride, x2 = x - stride, x3 = x + stride, x4 = x + stride;
		int y1 = y - stride, y2 = y + stride, y3 = y - stride, y4 = y + stride;
		array[x][y] = (array[x1][y1] + array[x2][y2] + array[x3][y3] + array[x4][y4] / 4.0)
				+ (rand.nextDouble() * maxRough);
	}

	/*
	 * +-+-+-+ | |1| | +-+-+-+ |2|M|3| +-+-+-+ | |4| | +-+-+-+
	 */
	public static void square(double[][] array, double maxRough, int x, int y, int stride) {
		int x1, x2, x3, x4;
		int y1, y2, y3, y4;
		x1 = x - stride;
		y1 = y;
		x2 = x + stride;
		y2 = y;
		x3 = x;
		y3 = y - stride;
		x4 = x;
		y4 = y + stride;
		int[] check = valueCheck(array.length, x1, y1, x2, y2, x3, y3, x4, y4);
		x1 = check[0];
		y1 = check[1];
		x2 = check[2];
		y2 = check[3];
		x3 = check[4];
		y3 = check[5];
		x4 = check[6];
		y4 = check[7];
		array[x][y] = (array[x1][y1] + array[x2][y2] + array[x3][y3] + array[x4][y4] / 4.0)
				+ (rand.nextDouble() * maxRough);
	}

	public static int[] valueCheck(int length, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
		if (x1 < 0)
			x1 += length;
		if (y1 < 0)
			y1 += length;
		if (x1 >= length)
			x1 -= length;
		if (y1 >= length)
			y1 -= length;

		if (x2 < 0)
			x2 += length;
		if (y2 < 0)
			y2 += length;
		if (x2 >= length)
			x2 -= length;
		if (y2 >= length)
			y2 -= length;

		if (x3 < 0)
			x3 += length;
		if (y3 < 0)
			y3 += length;
		if (x3 >= length)
			x3 -= length;
		if (y3 >= length)
			y3 -= length;

		if (x4 < 0)
			x4 += length;
		if (y4 < 0)
			y4 += length;
		if (x4 >= length)
			x4 -= length;
		if (y4 >= length)
			y4 -= length;
		return new int[] { x1, y1, x2, y2, x3, y3, x4, y4 };
	}
}
