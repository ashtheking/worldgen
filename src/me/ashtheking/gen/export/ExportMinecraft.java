package me.ashtheking.gen.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import me.ashtheking.gen.MidpointGen;
import me.ashtheking.gen.MidpointGen.ColorDefine;
import me.ashtheking.gen.MidpointGen.Tile;
import me.ashtheking.gen.export.Exporter.Export;
import me.ashtheking.gen.export.minecraft.NBT_Tag;
import me.ashtheking.gen.export.minecraft.TAG_Byte;
import me.ashtheking.gen.export.minecraft.TAG_Byte_Array;
import me.ashtheking.gen.export.minecraft.TAG_Compound;
import me.ashtheking.gen.export.minecraft.TAG_Double;
import me.ashtheking.gen.export.minecraft.TAG_Int;
import me.ashtheking.gen.export.minecraft.TAG_Int_Array;
import me.ashtheking.gen.export.minecraft.TAG_List;
import me.ashtheking.gen.export.minecraft.TAG_Long;
import me.ashtheking.gen.export.minecraft.TAG_String;

public class ExportMinecraft extends Export
{

	@Override
	public void export(String filepath, Tile[][] world, String name, String desc) {
		File saveDir = new File(filepath);
		if (!saveDir.exists())
			saveDir.mkdir();
		System.out.println(filepath);
		createLevel(saveDir, name, world.length);
		Object[] convert = convert(world);
		createWorld(saveDir, world, convert);
	}

	protected void createLevel(File saveDir, String name, int size) {
		TAG_Compound root = new TAG_Compound("");
		TAG_Compound data = new TAG_Compound("Data");
		root.elements.add(data);
		TAG_Int version = new TAG_Int("version");
		version.value = 19133;
		data.elements.add(version);
		TAG_Byte init = new TAG_Byte("initalized");
		init.value = 1;
		data.elements.add(init);
		TAG_String mapname = new TAG_String("LevelName");
		mapname.value = name;
		data.elements.add(mapname);
		TAG_String gen = new TAG_String("generatorName");
		gen.value = "default";
		data.elements.add(gen);
		TAG_Int genv = new TAG_Int("generatorVersion");
		genv.value = 0;
		data.elements.add(genv);
		TAG_String options = new TAG_String("generatorOptions");
		options.value = "";
		data.elements.add(options);
		TAG_Long seed = new TAG_Long("RandomSeed");
		seed.value = MidpointGen.SEED;
		data.elements.add(seed);
		TAG_Byte feature = new TAG_Byte("MapFeatures");
		feature.value = 1;
		data.elements.add(feature);
		data.elements.add(new TAG_Long("LastPlayed"));
		data.elements.add(new TAG_Long("SizeOnDisk"));
		TAG_Byte cheat = new TAG_Byte("allowCommands");
		cheat.value = 1;
		data.elements.add(cheat);
		data.elements.add(new TAG_Byte("hardcore"));
		TAG_Int gametype = new TAG_Int("GameType");
		gametype.value = 1;
		data.elements.add(gametype);
		data.elements.add(new TAG_Byte("Difficulty"));
		data.elements.add(new TAG_Byte("DifficultyLocked"));
		data.elements.add(new TAG_Long("Time"));
		data.elements.add(new TAG_Long("DayTime"));
		TAG_Int spawnx = new TAG_Int("SpawnX");
		spawnx.value = size / 2;
		data.elements.add(spawnx);
		TAG_Int spawny = new TAG_Int("SpawnY");
		spawny.value = 255;
		data.elements.add(spawny);
		TAG_Int spawnz = new TAG_Int("SpawnZ");
		spawnz.value = size / 2;
		data.elements.add(spawnz);
		data.elements.add(new TAG_Double("BorderCenterX"));
		data.elements.add(new TAG_Double("BorderCenterZ"));
		TAG_Double borderSize = new TAG_Double("BorderSize");
		borderSize.value = size * 2;
		data.elements.add(borderSize);
		TAG_Double borders = new TAG_Double("BorderSafeZone");
		borders.value = 5;
		data.elements.add(borders);
		TAG_Double borderw = new TAG_Double("BorderWarningBlocks");
		borderw.value = 5;
		data.elements.add(borderw);
		TAG_Double bordert = new TAG_Double("BorderWarningTime");
		bordert.value = 15;
		data.elements.add(bordert);
		TAG_Double borderlt = new TAG_Double("BorderSizeLerpTarget");
		borderlt.value = size * 2;
		data.elements.add(borderlt);
		data.elements.add(new TAG_Double("BorderSizeLerpTime"));
		TAG_Double borderd = new TAG_Double("BorderDamagePerBlock");
		borderd.value = size * 2;
		data.elements.add(borderd);
		data.elements.add(new TAG_Byte("raining"));
		TAG_Int rainTime = new TAG_Int("rainTime");
		rainTime.value = MidpointGen.RANDOM.nextInt(10000);
		data.elements.add(rainTime);
		data.elements.add(new TAG_Byte("thundering"));
		TAG_Int thunderTime = new TAG_Int("thunderTime");
		thunderTime.value = MidpointGen.RANDOM.nextInt(10000);
		data.elements.add(thunderTime);
		TAG_Int clearTime = new TAG_Int("clearWeatherTime");
		clearTime.value = MidpointGen.RANDOM.nextInt(100000);
		data.elements.add(clearTime);
		data.elements.add(new TAG_Compound("Player"));
		TAG_Compound rules = new TAG_Compound("GameRules");
		data.elements.add(rules);
		TAG_String blockOut = new TAG_String("commandBlockOutput");
		blockOut.value = "True";
		rules.elements.add(blockOut);
		TAG_String cycle = new TAG_String("doDaylightCycle");
		cycle.value = "True";
		rules.elements.add(cycle);
		TAG_String fire = new TAG_String("doFireTick");
		fire.value = "True";
		rules.elements.add(fire);
		TAG_String mobloot = new TAG_String("doMobLoot");
		mobloot.value = "True";
		rules.elements.add(mobloot);
		TAG_String mobspawn = new TAG_String("doMobSpawning");
		mobspawn.value = "True";
		rules.elements.add(mobspawn);
		TAG_String tiledrop = new TAG_String("doTileDrops");
		tiledrop.value = "True";
		rules.elements.add(tiledrop);
		TAG_String keepinv = new TAG_String("keepInventory");
		keepinv.value = "False";
		rules.elements.add(keepinv);
		TAG_String grief = new TAG_String("mobGriefing");
		grief.value = "True";
		rules.elements.add(grief);
		TAG_String regen = new TAG_String("naturalRegeneration");
		regen.value = "True";
		rules.elements.add(regen);
		TAG_String death = new TAG_String("showDeathMessages");
		death.value = "True";
		rules.elements.add(death);
		TAG_String rndtick = new TAG_String("randomTickSpeed");
		rndtick.value = "3";
		rules.elements.add(rndtick);

		try {
			File file = new File(saveDir, "level.dat");
			if (!file.exists())
				file.createNewFile();
			GZIPOutputStream stream = new GZIPOutputStream(new FileOutputStream(file));
			root.save(stream);
			stream.close();
			GZIPInputStream stream2 = new GZIPInputStream(new FileInputStream(file));
			root = (TAG_Compound) NBT_Tag.make(stream2);
			stream2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getName() {
		return "Minecraft Exporter";
	}

	@Override
	public String getExtension() {
		return "\\";
	}

	protected void createWorld(File saveDir, Tile[][] world, Object[] convert) {
		File dataDir = new File(saveDir, "region/");
		dataDir.mkdir();
		for (int cz = 0; cz < world.length; cz += 16)
			for (int cx = 0; cx < world.length; cx += 16) {
				int posx = (cz - world.length / 2) / 16;
				int posy = (cx - world.length / 2) / 16;
				String filename = "r." + posx + "." + posy + ".mca";
				createChunk(dataDir, world, convert, cz, cx, filename);
			}
	}

	private void createChunk(File dataDir, Tile[][] world, Object[] convert, int cz, int cx, String filename) {
		TAG_Compound root = new TAG_Compound("");
		{
			TAG_Compound level = new TAG_Compound("Level");
			{
				TAG_Int xpos = new TAG_Int("xPos");
				xpos.value = (cx - world.length / 2) / 16;
				level.elements.add(xpos);
				TAG_Int zpos = new TAG_Int("zPos");
				zpos.value = (cz - world.length / 2) / 16;
				level.elements.add(zpos);
				TAG_Long update = new TAG_Long("LastUpdate");
				update.value = new Date().getTime();
				level.elements.add(update);
				level.elements.add(new TAG_Byte("LightPopulated"));
				level.elements.add(new TAG_Byte("TerrainPopulated"));
				level.elements.add(new TAG_Byte("V"));
				level.elements.add(new TAG_Long("InhabitedTime"));
				TAG_Int_Array heightmap = new TAG_Int_Array("HeightMap");
				{
					int[][] height = (int[][]) convert[1];
					heightmap.data = new int[height.length * height.length];
					for (int z = 0; z < 16; z++)
						for (int x = 0; x < 16; x++)
							if (cz + z < height.length && cx + x < height.length)
								if (z * 16 + x < heightmap.data.length)
									heightmap.data[(z * 16) + x] = height[cz + z][cx + x];
				}
				level.elements.add(heightmap);
				TAG_List sections = new TAG_List("Sections");
				{
					List<NBT_Tag> list = new ArrayList<NBT_Tag>();
					byte[][][] map = (byte[][][]) convert[0];
					for (int cy = 0; cy < 256; cy += 16) {
						TAG_Compound sec = new TAG_Compound("");
						{
							TAG_Byte ypos = new TAG_Byte("Y");
							ypos.value = (byte) (cy / 16);
							sec.elements.add(ypos);
							TAG_Byte_Array blocks = new TAG_Byte_Array("Blocks");
							blocks.data = new byte[4096];
							for (int y = 0; y < 16; y++)
								for (int z = 0; z < 16; z++)
									for (int x = 0; x < 16; x++) {
										int loc = (y * 16 * 16) + (z * 16) + x;
										if (loc > 4095)
											continue;
										if (cy + y >= map.length)
											continue;
										if (cz + z >= map[cy + y].length)
											continue;
										if (cx + x >= map[cy + y][cz + z].length)
											continue;
										blocks.data[loc] = map[cy + y][cz + z][cx + x];
									}
							sec.elements.add(blocks);
							TAG_Byte_Array data = new TAG_Byte_Array("Data");
							data.data = new byte[2048];
							sec.elements.add(data);
							TAG_Byte_Array light = new TAG_Byte_Array("BlockLight");
							light.data = new byte[2048];
							sec.elements.add(light);
							TAG_Byte_Array sky = new TAG_Byte_Array("SkyLight");
							sky.data = new byte[2048];
							sec.elements.add(sky);
						}
						//sections.elements[cy / 16] = sec;
						list.add(sec);
					}
					sections.elements = list.toArray(new NBT_Tag[list.size()]);
				}
				level.elements.add(sections);
				//				TAG_List entity = new TAG_List("Entities");
				//				TAG_Byte ente = new TAG_Byte("");
				//				entity.elements = new TAG_Byte[] {ente};
				//				level.elements.add(entity);
				//				TAG_List tile = new TAG_List("TileEntities");
				//				TAG_Byte tilee = new TAG_Byte("");
				//				entity.elements = new TAG_Byte[] {tilee};
				//				level.elements.add(tile);
			}
			root.elements.add(level);
		}
		try {
			File file = new File(dataDir, filename);
			if (!file.exists())
				file.createNewFile();
			GZIPOutputStream stream = new GZIPOutputStream(new FileOutputStream(file));
			root.save(stream);
			stream.close();
			GZIPInputStream stream2 = new GZIPInputStream(new FileInputStream(file));
			root = (TAG_Compound) NBT_Tag.make(stream2);
			stream2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object[] convert(Tile[][] world) {
		byte[][][] map = new byte[256][world.length][world.length];
		int[][] height = new int[world.length][world.length];
		for (int y = 0; y < map.length; y++) {
			for (int z = 0; z < world.length; z++)
				for (int x = 0; x < world[z].length; x++) {
					if (world[z][x].height > y)
						map[y][z][x] = 1;
					else if (world[z][x].height == y) {
						if (world[z][x].moisture < 33)
							map[y][z][x] = 12;
						else if (world[z][x].temp < 33)
							map[y][z][x] = 78;
						else
							map[y][z][x] = 2;
						height[z][x] = y;
					}
					else if (world[z][x].height < y)
						map[y][z][x] = 0;
					if (world[z][x].ocean)
						if (map[y][z][x] == 0 && y < ColorDefine.ocean)
							map[y][z][x] = 8;
					if (world[z][x].lake || world[z][x].river)
						if (map[y][z][x] == 0) {
							map[y][z][x] = 8;
							world[z][x].lake = false;
							world[z][x].river = false;
						}
					if (y == 0)
						map[y][z][x] = 7;
				}
		}
		return new Object[] { map, height };
	}

	@Override
	public String getDefaultSave() {
		String os = System.getProperty("os.name");
		if (os.contains("windows") || os.contains("Windows"))
			return "%appdata%/.minecraft/saves/";
		return "~/.minecraft/saves/";
	}

}
