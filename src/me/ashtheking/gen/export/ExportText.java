package me.ashtheking.gen.export;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import me.ashtheking.gen.Configuration;
import me.ashtheking.gen.MidpointGen.Tile;
import me.ashtheking.gen.export.Exporter.Export;

public class ExportText extends Export
{

	@Override
	public String getName() {
		return "Exporter to .txt";
	}

	@Override
	public String getExtension() {
		return ".txt";
	}

	@Override
	public void export(String filepath, Tile[][] world, String name, String desc) {
		try {
			File out = new File(filepath);
			PrintStream print = new PrintStream(new FileOutputStream(out));
			char[][] display = new char[world.length][world.length];
			int t = 0;
			double scale = Configuration.config.getDoubleProperty("height-max", 1)
					- Configuration.config.getDoubleProperty("height-min", 0);

			for (int x = 0; x < world.length; x++)
				for (int y = 0; y < world.length; y++) {
					double k = (t == 0 ? world[x][y].height : (t == 1 ? world[x][y].temp
							: (t == 2 ? world[x][y].drainage : world[x][y].rainfall)))
							- Configuration.config.getDoubleProperty("height-min", 0);
					if (k < scale * 0.4)
						display[x][y] = '~';
					else if (k >= scale * 0.4 && k < scale * 0.45)
						display[x][y] = ',';
					else if (k >= scale * 0.45 && k < scale * 0.6)
						display[x][y] = '.';
					else if (k >= scale * 0.6 && k < scale * 0.7)
						display[x][y] = '#';
					else if (k >= scale * 0.7 && k < scale * 0.8)
						display[x][y] = '/';
					else if (k >= scale * 0.8 && k < scale * 0.9)
						display[x][y] = '^';
					else if (k >= scale * 0.9)
						display[x][y] = '`';
				}

			for (int x = 0; x < world.length; x++) {
				for (int y = 0; y < world.length; y++) {
					print.print(display[x][y]);
				}
				print.println();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public String getDefaultSave() {
		// TODO Auto-generated method stub
		return null;
	}

}
