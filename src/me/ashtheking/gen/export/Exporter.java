package me.ashtheking.gen.export;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import me.ashtheking.gen.MidpointGen.Tile;

public class Exporter
{
	public static List<Export> list = new ArrayList<Export>();

	public static void init() {
		try {
			File cfg = new File("export.cfg");
			if (!cfg.exists()) {
				cfg.createNewFile();
				return;
			}
			Scanner scan = new Scanner(cfg);
			if (!scan.hasNext())
				return;
			String str;
			do {
				str = scan.nextLine();
				Object obj = Class.forName(str).newInstance();
				list.add((Export) obj);
				System.out.println("Loaded Export: " + str + " (" + ((Export) obj).getName() + ")");
			} while (scan.hasNext());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showDialog(Tile[][] world) {
		if (list.size() < 1) {
			JOptionPane.showMessageDialog(null, "No Exporters Found!");
			return;
		}
		Export o = (Export) JOptionPane.showInputDialog(null, "Export to?", "Export", JOptionPane.INFORMATION_MESSAGE,
				null, list.toArray(), list.get(0));
		String out = "export" + o.getExtension();
		JFileChooser chose = new JFileChooser(o.getDefaultSave());
		if(o.getExtension().equals("\\"))
			chose.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Minecraft (Folder)", o.getExtension());
		chose.setFileFilter(filter);
		if (chose.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			out = chose.getSelectedFile().getAbsolutePath() + o.getExtension();
		o.export(out, world, JOptionPane.showInputDialog("World Name?"),
				JOptionPane.showInputDialog("World Description?") + " | Exported by WorldGen (ashtheking 2014)");
		System.out.println("Exported via " + o);
	}

	public static abstract class Export
	{
		public abstract void export(String filepath, Tile[][] world, String name, String desc);

		public abstract String getName();

		public abstract String getExtension();

		public abstract String getDefaultSave();

		@Override
		public String toString() {
			return getName();
		}
	}
}
