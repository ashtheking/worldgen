package me.ashtheking.gen.export;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import me.ashtheking.gen.MidpointGen.Tile;
import me.ashtheking.gen.export.Exporter.Export;

public class ExportCivilizationV extends Export
{

	public static String[] terrain = new String[] { "TERRAIN_GRASS", "TERRAIN_PLAINS", "TERRAIN_DESERT",
			"TERRAIN_TUNDRA", "TERRAIN_SNOW", "TERRAIN_COAST", "TERRAIN_OCEAN" };
	public static String[] feature1 = new String[] { "FEATURE_ICE", "FEATURE_JUNGLE", "FEATURE_MARSH", "FEATURE_OASIS",
			"FEATURE_FLOOD_PLAINS", "FEATURE_FOREST", "FEATURE_FALLOUT", "FEATURE_ATOLL" };
	public static String[] resource = new String[] { "RESOURCE_IRON", "RESOURCE_HORSE", "RESOURCE_COAL",
			"RESOURCE_OIL", "RESOURCE_ALUMINUM", "RESOURCE_URANIUM", "RESOURCE_WHEAT", "RESOURCE_COW",
			"RESOURCE_SHEEP", "RESOURCE_DEER", "RESOURCE_BANANA", "RESOURCE_FISH", "RESOURCE_STONE", "RESOURCE_WHALE",
			"RESOURCE_PEARLS", "RESOURCE_GOLD", "RESOURCE_SILVER", "RESOURCE_GEMS", "RESOURCE_MARBLE",
			"RESOURCE_IVORY", "RESOURCE_FUR", "RESOURCE_DYE", "RESOURCE_SPICES", "RESOURCE_SILK", "RESOURCE_SUGAR",
			"RESOURCE_COTTON", "RESOURCE_WINE", "RESOURCE_INCENSE" };

	@Override
	public void export(String filepath, Tile[][] world, String name, String desc) {
		try {
			File output = new File(filepath);
			DataOutputStream data = new DataOutputStream(new FileOutputStream(output));
			String mapName = name;

			data.writeByte(10);
			data.writeInt(swapEndian(world.length));
			data.writeInt(swapEndian(world[0].length));
			data.writeByte(0);
			data.writeInt(swapEndian(7));
			data.writeInt(swapEndian(100));
			data.writeInt(swapEndian(121));
			data.writeInt(swapEndian(0));
			data.writeInt(swapEndian(417));
			data.writeInt(swapEndian(0));
			data.writeInt(swapEndian(mapName.length()));
			data.writeInt(swapEndian(desc.length()));
			for(String s : terrain) {
				for(char c : s.toCharArray())
					data.writeByte((byte)c);
				data.writeByte(0x00);
			}
			for(String s : feature1) {
				for(char c : s.toCharArray())
					data.writeByte((byte)c);
				data.writeByte(0x00);
			}
			data.writeByte(0x00);
			for(String s : resource) {
				for(char c : s.toCharArray())
					data.writeByte((byte)c);
				data.writeByte(0x00);
			}
			for(char c : mapName.toCharArray())
				data.writeByte((byte)c);
			for(char c : desc.toCharArray())
				data.writeByte((byte)c);
			for(char c : "WORLDSIZE_TINY".toCharArray())
				data.writeByte((byte)c);
			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public String getName() {
		return "Civilization V (Civ 5) Exporter";
	}

	@Override
	public String getExtension() {
		return ".civ5map";
	}

	public static int highNybble(byte b) {
		return (b >> 4) & 0x0f;
	}

	public static int lowNybble(byte b) {
		return b & 0x0f;
	}

	public static int swapEndian(int i) {
		return ((i & 0xff) << 24) + ((i & 0xff00) << 8) + ((i & 0xff0000) >> 8) + ((i >> 24) & 0xff);
	}

	@Override
	public String getDefaultSave() {
		return null;
	}
}
